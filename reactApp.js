// I was in the room with Jason as he got assistance from Leanne Benson on this project.  I changed my original code to her version to make adding the medium conditions in easier.  I also spent a lot of time in the open Zoom room absorbing information.

// I found this assessment very challenging and was very thankful for all the study groups.

// I decided to not do any CSS so I can focus on moving forward in the lesson.  I dont want to get behind because I spent too much time playing with CSS.

class Team extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shots: 0,
      score: 0,
    };
    this.dribbleSound = new Audio("./basket.wav");
    this.swishSound                                                = new Audio("./Swish.wav");
  }

  shootHandler = () => {
    let score = this.state.score;
    this.dribbleSound.play();
    if (Math.random() >= 0.33) {
      score += 1;
    }
    this.setState(() => ({
      shots: this.state.shots + 1,
      score,
    }));
  };

  render() {
    let accuracy;
    if (this.state.shots) {
      const shotPercentage = Math.round(
        (this.state.score / this.state.shots) * 100
      );
      accuracy = <div>Your Shot is Accuracy %: {shotPercentage}</div>;
    }

    return (
      <div className="team">
        <h2>{this.props.name}</h2>
        <div className="teamNames">
          <img src={this.props.img} alt={this.props.name} />
        </div>
        <div>Shots Taken:{this.state.shots}</div>
        <div>Score:{this.state.score}</div>
        {accuracy}
        <button onClick={this.shootHandler}>Take a Shot!</button>
      </div>
    );
  }
}

function Match(props) {
  return (
    <div className="match">
      <h1>Welcome to {props.venue}</h1>
      <div className="teamStats">
        <Team name={props.awayTeam.name} img={props.awayTeam.imgSRC} />
        <div className="versus">
          <h1>VERSUS</h1>
        </div>
        <Team name={props.homeTeam.name} img={props.homeTeam.imgSRC} />
      </div>
    </div>
  );
}

function App() {
  const Avalanche = {
    name: "Avalanche",
    imgSRC: "./Avalanch.png",
  };

  const Quakes = {
    name: "Quakes",
    imgSRC: "./quake.png",
  };

  const Tornados = {
    name: "Tornados",
    imgSRC: "./Tornado.png",
  };

  const Volcanoes = {
    name: "Volcanoes",
    imgSRC: "./volcano.png",
  };

  return (
    <div className="App">
      <Match homeTeam={Avalanche} awayTeam={Quakes} venue="Mile High Stadium" />
      <Match homeTeam={Tornados} awayTeam={Volcanoes} venue="Staples Arena" />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
